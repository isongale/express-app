import dotenv from 'dotenv';
import express from 'express';
import expressSanitizer from 'express-sanitizer';
import FeedController from './controllers/feed.controller.js';
import ErrorHandlingMiddleware from './middleware/error-handling.middleware.js';
import log from './utils/logger.util.js';

// Load .env configuration
dotenv.config();

const PORT = process.env.PORT || 3000;
const app = express();

// Registration of expressSanitizer middleware
app.use(expressSanitizer());

app.use('/', FeedController);

// Registration of error middleware
ErrorHandlingMiddleware(app);

app.listen(PORT, () => {
  log.info(`Server listening on port ${PORT}`);
  console.log(`Server listening on port ${PORT}`);
});
