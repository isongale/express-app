import RssValidationError from '../errors/rss-validation.error.js';
import TheguardianError from '../errors/theguardian.error.js';
import log from '../utils/logger.util.js';

/**
 * Manage Rss Validation Error
 * @param {*} err
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
function rssValidationErrorHandler (err, req, res, next) {
  if (err instanceof RssValidationError) {
    log.error({ err: err }, 'rssValidationErrorHandler');
    return res.sendStatus(503);
  }
  next(err);
}

/**
 * Manage The Guardian API Error
 * @param {*} err
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
function theguardianErrorErrorHandler (err, req, res, next) {
  if (err instanceof TheguardianError) {
    log.error({ err: err }, 'theguardianErrorErrorHandler');
    return res.sendStatus(503);
  }
  next(err);
}

/**
 * Manage a generic error
 * @param {*} _err
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
function genericErrorHandler (err, req, res, next) {
  log.error({ err: err }, 'genericErrorHandler');
  res.sendStatus(500);
}

export default function ErrorHandlingMiddleware (app) {
  app.use([
    rssValidationErrorHandler,
    theguardianErrorErrorHandler,
    genericErrorHandler
  ]);
};
