export default class RssValidationError {
  constructor (message) {
    this.message = message;
  }
};
