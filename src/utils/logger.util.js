import bunyan from 'bunyan';
import dotenv from 'dotenv';

// Load .env configuration
dotenv.config();

const logPath = process.env.LOG_DIR + 'express-app.log';

const log = bunyan.createLogger({
  name: 'express-app',
  streams: [{
    type: 'rotating-file',
    path: logPath, // path to log folder
    period: '1d', // daily rotation
    count: 3, // keep 3 back copies
    level: process.env.LOG_LEVEL
  }]
});

export default log;
