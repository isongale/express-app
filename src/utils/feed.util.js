import bent from 'bent';
import RSS from 'rss-generator';
import XML from 'pixl-xml';
import log from '../utils/logger.util.js';

export default class FeedUtil {
  /**
   * Create RSS feed
   * @param {*} objArticleArray
   * @param {*} category
   */
  async createFeedFromObj (objArticleArray, category) {
    const feedObj = new RSS({
      'title': category + ' news | Feed Title',
      'description': category + ' news | Feed Description',
      'link': 'http://www.example.com/'
    });
    if (objArticleArray.length > 0) {
      objArticleArray.forEach((article) => {
        feedObj.item({
          'title': article.title,
          'link': article.url,
          'description': article.trailText,
          'date': article.date
        });
      });
    } else {
      log.warn(`No element for category ${category}`);
    }

    const xmlFeed = feedObj.xml({ indent: true });

    return xmlFeed;
  }

  /**
   * Validate RSS feed
   * @param {*} xmlFeed
   */
  async feedValidation (xmlFeed) {
    // call feed validation
    const validationResponse = await this.callValidator(xmlFeed);

    // parse feed validation message
    const isValid = await this.checkValidation(validationResponse);

    return isValid;
  }

  /**
   * Call validator service.
   * More info at https://validator.w3.org/feed/docs/soap.html
   * @param {*} xmlFeed
   */
  async callValidator (xmlFeed) {
    const VALIDATOR_URL = 'http://validator.w3.org/feed/check.cgi';
    const headers = {
      'Content-type': 'application/x-www-form-urlencoded'
    };

    const xmlBUffer = Buffer.from('manual=1&output=soap12&rawdata=' + encodeURIComponent(xmlFeed));

    // get external API response
    const getValidation = bent('POST', headers, 'string');
    const validationResponse = await getValidation(VALIDATOR_URL, xmlBUffer);

    return validationResponse;
  }

  /**
   * Check validation response
   * @param {*} validationResponse
   */
  async checkValidation (validationResponse) {
    const validationResponseString = XML.parse(validationResponse);
    const isValid = validationResponseString['env:Body']['m:feedvalidationresponse']['m:validity'];
    const isValidBool = (isValid === 'true');

    return isValidBool;
  }
}
