import express from 'express';
import FeedService from '../services/feed.service.js';
import log from '../utils/logger.util.js';

const router = express.Router({ 'caseSensitive': true });
const feedService = new FeedService();

/**
 *  Only lowercase letter are allowed, otherwise return a status code 400
 */
router.param('section', function (req, res, next, section) {
  if (section.match(/^(?:([a-z]+))\/?$/) !== null) {
    next();
  } else {
    res.sendStatus(400);
  }
});

/**
 * Return rss feed by section
 */
router.get('/:section', async (req, res, next) => {
  log.debug(req, 'Start request');
  const section = req.sanitize(req.params.section);
  try {
    const feed = await feedService.getFeed(section);
    res.contentType('text/xml');
    res.send(feed);
  } catch (e) {
    log.error(e);
    next(e);
  }
  log.debug(req, 'End request');
});

export default router;
