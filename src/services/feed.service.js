import TheguardianService from './theguardian.service.js';
import FeedUtil from '../utils/feed.util.js';
import RssValidationError from '../errors/rss-validation.error.js';

export default class FeedService {
  /**
   * Get feed by category
   * @param {*} category
   */
  async getFeed (category) {
    const theguardianService = new TheguardianService();
    const feedUtil = new FeedUtil();

    // Get external API response with news
    const obj = await theguardianService.getArticles(category);

    // Create obj with data
    const objArticleArray = await theguardianService.parseArticles(obj.response.results);

    // Create rss feed
    const feed = await feedUtil.createFeedFromObj(objArticleArray, category);

    // Validate rss feed
    const isValid = await feedUtil.feedValidation(feed);
    if (!isValid) {
      throw new RssValidationError('RSS validation error');
    }

    return feed;
  }
}
