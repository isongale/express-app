import bent from 'bent';
import TheguardianError from '../errors/theguardian.error.js';

export default class TheguardianService {
  /**
   * Get article from The Guardian API by section
   * @param {*} section
   */
  async getArticles (section) {
    const url = process.env.THEGUARDIAN_URL + section;

    const headers = {
      'api-key': process.env.THEGUARDIAN_KEY
    };

    try {
      // get external API response
      const getJSON = bent(headers, 'json');
      const articleJSON = await getJSON(url);

      return articleJSON;
    } catch (e) {
      throw new TheguardianError('The Guardian Error');
    }
  }

  /**
   * Parse articles and build an array with all the necessary info
   * @param {*} articleJSON
   */
  async parseArticles (articleJSON) {
    const articleArray = new Array(articleJSON.length);

    if (articleJSON.length > 0) {
      articleJSON.forEach((article) => {
        const currentElement = {
          'title': article.webTitle,
          'url': article.webUrl,
          'date': new Date(article.webPublicationDate),
          'trailText': article.fields.trailText,
        };
        articleArray.push(currentElement);
      });
    }
    return articleArray;
  }
}
