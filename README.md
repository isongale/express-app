# express-app
Feed RSS creator by The Guardian API


## Run in dev mode
With node:
node --experimental-modules src/app.js

Or with Babel:
npm start

## Dependencies

- [@babel/runtime](https://ghub.io/@babel/runtime): babel&#39;s modular runtime helpers
- [bent](https://ghub.io/bent): Functional HTTP client for Node.js w/ async/await.
- [bunyan](https://ghub.io/bunyan): a JSON logging library for node.js services
- [dotenv](https://ghub.io/dotenv): Loads environment variables from .env file
- [express](https://ghub.io/express): Fast, unopinionated, minimalist web framework
- [express-sanitizer](https://ghub.io/express-sanitizer): Express middleware for the sanitizer module.
- [pixl-xml](https://ghub.io/pixl-xml): A simple module for parsing and composing XML.
- [rss-generator](https://ghub.io/rss-generator): RSS feed generator. Add RSS feeds to any project. Supports enclosures and GeoRSS.

## Dev Dependencies

- [@babel/cli](https://ghub.io/@babel/cli): Babel command line.
- [@babel/core](https://ghub.io/@babel/core): Babel compiler core.
- [@babel/node](https://ghub.io/@babel/node): Babel command line
- [@babel/plugin-transform-async-to-generator](https://ghub.io/@babel/plugin-transform-async-to-generator): Turn async functions into ES2015 generators
- [@babel/plugin-transform-runtime](https://ghub.io/@babel/plugin-transform-runtime): Externalise references to helpers and builtins, automatically polyfilling your code without polluting globals
- [@babel/preset-env](https://ghub.io/@babel/preset-env): A Babel preset for each environment.
- [eslint](https://ghub.io/eslint): An AST-based pattern checker for JavaScript.
- [eslint-config-standard](https://ghub.io/eslint-config-standard): JavaScript Standard Style - ESLint Shareable Config
- [eslint-plugin-import](https://ghub.io/eslint-plugin-import): Import with sanity.
- [eslint-plugin-node](https://ghub.io/eslint-plugin-node): Additional ESLint&#39;s rules for Node.js
- [eslint-plugin-promise](https://ghub.io/eslint-plugin-promise): Enforce best practices for JavaScript promises
- [eslint-plugin-standard](https://ghub.io/eslint-plugin-standard): ESlint Plugin for the Standard Linter